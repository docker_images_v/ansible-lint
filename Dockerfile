FROM python:3.9-slim-bullseye

RUN apt-get update &&\
    apt-get clean &&\
    export ANSIBLE_HOST_KEY_CHECKING=False &&\
    apt-get install ansible-lint -y
